package com.nt.examples

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object ArrayFile extends App {

  val spark = SparkSession.builder().appName("Array").master("local[*]").getOrCreate()

  case class Message1(others: String, text: Array[String])

  val r3 = Message1("foo1", Array("a", "b", "c"))
  val r4 = Message1("foo2", Array("d", "e", "f"))
  import spark.implicits._
  val records1 = Seq(r3, r4)
  val df1 = spark.createDataFrame(records1)
  df1.show()
  df1.withColumn("col1", (col("text")).getItem(0))
    .withColumn("col2", (col("text")).getItem(1))
    .withColumn("col3", (col("text")).getItem(2)).drop("text")

}